import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InsertComponent } from './insert/insert.component';

const routes: Routes = [
  { path: '', redirectTo: 'insert', pathMatch: 'full'},
  { path: 'insert', component: InsertComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudRoutingModule { }
