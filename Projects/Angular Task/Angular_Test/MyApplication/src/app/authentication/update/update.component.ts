import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrls: ['./update.component.scss']
})
export class UpdateComponent implements OnInit {

  productForm!: FormGroup;

  constructor(
    public fb: FormBuilder,
    public router : Router,
  ) { }

  ngOnInit(): void {
    this.productForm = this.fb.group({
      name:['']
    })
  }

  submitForm(){
    console.log("test");
    console.log(this.productForm.value);
    
  }

}
