import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  login : any=FormGroup
  constructor
 
  (
    public formbuilder: FormBuilder,
    public router: Router,
    public route: ActivatedRoute

  ) { }

  ngOnInit(): void {
    this.login = this.formbuilder.group({
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)])
    })  
  }

  get f(){
    return this.login.controls;
  }

  loginform(){
    
    console.log(this.login.value.email);
    let getdatas = JSON.parse(localStorage.getItem('data') || '{}');
   
    const user = (this.login.value.email === getdatas.email && this.login.value.password === getdatas.password);
    if(!user){
      console.log('Username or password is incorrect');
      return;
    }
    else{
      this.router.navigateByUrl('/ecommerce/home');
    } 
  }

}

