import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Product } from '../product';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  products: Product[] = [];

  constructor(
    public httpClient: HttpClient,
    public CategoryService: CategoryService
  ) { }

  ngOnInit(): void {
    this.listProducts();
  }

  listProducts() {
    this.httpClient.get(`https://fakestoreapi.com/products`).subscribe((data: any) => {
      console.log(data);
      this.products = data;
    });
  }

  addtocart(item: any){
    this.CategoryService.addtoCart(item);
  }

}
