import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CategoryService } from '../category.service';
import { Product } from '../product';


@Component({
  selector: 'app-category-listing',
  templateUrl: './category-listing.component.html',
  styleUrls: ['./category-listing.component.scss']
})
export class CategoryListingComponent implements OnInit {
  productForm!: FormGroup;

  ngOnInit(): void {
    this.productForm = this.fb.group({
      name:[''],
      category:[''],
    })
  }

  constructor( 
    public fb: FormBuilder,
    public router: Router,
    public CategoryService: CategoryService,
    public httpClient: HttpClient,
    public route: ActivatedRoute,
    public modalService: NgbModal,
    ) { }
    
    submitForm()
    {
      console.log("test");
      console.log(this.productForm.value);  
    }

    back(){
      this.router.navigateByUrl('/ecommerce/home');
    }

}
