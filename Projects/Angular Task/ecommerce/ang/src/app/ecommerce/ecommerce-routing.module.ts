import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ProductComponent } from './product/product.component';
import { ContactComponent } from './contact/contact.component';
import { TestimonialComponent } from './testimonial/testimonial.component';
import { CartComponent } from './cart/cart.component';
import { CategoryListingComponent } from './category-listing/category-listing.component';



const routes: Routes = [
  { path: '', redirectTo: 'ecommerce/home', pathMatch: 'full'},
  { path: 'ecommerce/home', component: HomeComponent },
  { path: 'ecommerce/about', component: AboutComponent },
  { path: 'ecommerce/product', component: ProductComponent },
  { path: 'ecommerce/contact', component: ContactComponent },
  { path: 'ecommerce/testimonial', component: TestimonialComponent },
  { path: 'ecommerce/cart', component: CartComponent },
  { path: 'ecommerce/category-list', component: CategoryListingComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EcommerceRoutingModule { }
