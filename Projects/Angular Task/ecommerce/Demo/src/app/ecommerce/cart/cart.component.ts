import { Component, OnInit } from '@angular/core';
import { CategoryService } from '../category.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public products : any = [];
  public grandTotal !: number;
  constructor(private CategoryService : CategoryService) { }

  ngOnInit(): void {
    this.CategoryService.getProducts()
    .subscribe(res=>{
      this.products = res;
      console.log(this.products ,"cart component");
      this.grandTotal = this.CategoryService.getTotalPrice();
    })
  }

}
