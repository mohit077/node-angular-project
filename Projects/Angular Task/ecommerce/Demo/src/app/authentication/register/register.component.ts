import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {
 

  registerdata : any=FormGroup;

  constructor(
    public formbuilder: FormBuilder,
    public router: Router,
    public route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.registerdata = this.formbuilder.group({
      name: new FormControl('', [Validators.required, Validators.minLength(3)]),
      email: new FormControl('', [Validators.required, Validators.email]),
      phone: new FormControl('', [Validators.required, Validators.minLength(10)]),
      password: new FormControl('', [Validators.required, Validators.minLength(5)])
    })
  }

  get f(){
    return this.registerdata.controls;
  }

  RegisterForm(){
      localStorage.setItem('data', JSON.stringify(this.registerdata.value));
      // const getdata = localStorage.getItem('data');
      // console.log(getdata); 
      // this.router.navigateByUrl('/authentication/login');
    
        this.router.navigateByUrl('/authentication/login');
     
  }

}
