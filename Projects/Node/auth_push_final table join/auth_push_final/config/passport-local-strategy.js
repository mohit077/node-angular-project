const passport = require('passport');

const LocalStrategry = require('passport-local').Strategy;

const User = require('../models/user');

passport.use(new LocalStrategry({
    usernameField : 'email'
},
    function(email,password,done){
        User.findOne({email : email}, function(err,user){
            if(err){
                console.log("error in finding passport data:");
                return done(err);
            }
            if(!user || user.password !=  password){
                return done(null,false);
            }
            return done(null,user);
        })
    }
))

passport.serializeUser(function(user,done){
    done(null,user.id);
});
  

passport.deserializeUser(function(id,done){
    User.findById(id, function(err,user){
        if(err){
            console.log("user data not found ");
            return false;
        }
        return done(null,user);
    });
});


passport.checkAuthentication = function(req,res,next)
    {
    if(req.isAuthenticated()){
        // console.log()
       return next();
    }
    return res.redirect('/login');
}


passport.setAuthenticatedUser = function(req,res,next)
{
    if(req.isAuthenticated()){
        // console.log(req.user);
        res.locals.user = req.user;
    }
    next();
}
module.exports = passport;