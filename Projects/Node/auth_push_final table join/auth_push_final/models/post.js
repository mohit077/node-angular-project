const mongoose = require('mongoose');
const user = require('./user');

const postSchema = mongoose.Schema({
    content :{
        type : String,
        required : true
    },
    user :{
        type : mongoose.Schema.Types.ObjectId,
        ref : 'users'  
    },
},{
    timestamps : true
});

const Post = mongoose.model('posts',postSchema);

module.exports = Post;