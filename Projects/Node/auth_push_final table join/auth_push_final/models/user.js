const mongoose = require('mongoose');

const multer = require('multer');

const path = require('path');

const AVATAR_PATH = path.join("/uploads/users");

const userSchema = mongoose.Schema({
    username : {
        required: true,
        type: String
    },
    email : {
        required : true,
        type : String
    },
    password : {
        required: true,
        type : String
    },
    avatar :{
        type : String
    }

});

const storage = multer.diskStorage({
    destination: function(req,file,cb){
        cb(null, path.join(__dirname,'..',AVATAR_PATH));
    },
    filename : function(req,file,cb){
        cb(null, file.fieldname+"-"+Date.now());
    }
});

userSchema.statics.uploadedAvatar = multer({storage : storage}).single('avatar');
userSchema.statics.avatarPath = AVATAR_PATH;


const user = mongoose.model('users',userSchema);

module.exports = user;