const express  = require('express');
const port = 8001;
const path = require('path');
const app = express();
const db = require('./config/mongoose');

const session = require('express-session');
const passport = require('passport');
const passportLocal = require('./config/passport-local-strategy');
const flash = require('connect-flash');
const customMware  = require('./config/middleware');
app.use(express.urlencoded());


app.use(express.static('/assets'));
app.use('/uploads',express.static(__dirname+"/uploads"));


app.set('view engine','ejs');
app.set('views', path.join(__dirname,'views'));

app.use(session({
    name : "codeial",
    secret : 'blahsometing',
    saveUninitialized : false,
    resave : false,
    cookie :{
        maxAge : (100 * 60 *10)
    }
}
));
app.use(passport.initialize());
app.use(passport.session());
app.use(passport.setAuthenticatedUser);

app.use(flash());
app.use(customMware.setFlash);


app.use('/', require('./routes'));

app.listen(port, function(err){
    if(err){
        console.log(err);
        return false;
    }
    console.log("server is running on port:",port);
});
