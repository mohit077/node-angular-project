const Post = require('../models/post');

module.exports.createPost = function(req,res){
    console.log("controller called");
    // console.log(req.body);
    Post.create({
        content : req.body.content,
        user : req.user._id
    },function(err, post){
        if(err){ 
            console.log("err");
        }
        req.flash('success','Post data inserted');
        return res.redirect('back');
    })
}