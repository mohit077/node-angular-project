const DataPost = require('../models/_post');

module.exports.postCreate = function(req,res){
   DataPost.create({
       content : req.body.content,
       user : req.user._id
   }, function(err, dataPost){
       if(err){
           console.log("someting wrong");
       }
       req.flash("success","Inserted success");
       return res.redirect('back');
   })
}