module.exports.profile=function(req,res){
    return res.render('profile');
}

module.exports.createSession = function(req,res){
    return res.redirect('/users/profile');
}

module.exports.destroySession = function(req,res){
    req.logout();
    return res.redirect('/users/login');
}