const mongoose = require('mongoose');

const postSchema = mongoose.Schema({
    content:{
        type : String,
        required : true
    },
    user : {
        type : mongoose.Schema.Types.ObjectId,
        ref : 'users'
    }
},
{
    timestamps : true
});

const post = mongoose.model('postsD', postSchema);

module.exports = post;