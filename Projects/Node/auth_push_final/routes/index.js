const express = require('express');
const passport = require('passport');

const routes = express.Router();

const homeController = require('../controllers/home_controller');
const route = require('./_post');
console.log("routing is used");

routes.get('/_postPage',  passport.checkAuthentication, homeController.postPage);

routes.get('/',  passport.checkAuthentication, homeController.home);

routes.get('/login', homeController.login);

routes.get('/register', homeController.register);

routes.post('/registerUser', homeController.registerUser);

routes.use('/users', require('./user'));

routes.use('/posts',require('./post'));

routes.use('/_posts', require('./_post'));

module.exports = routes;