const express = require('express');
const passport = require('passport');
const routes = express.Router();
const userController = require('../controllers/user_controller');


routes.get('/profile',passport.checkAuthentication, userController.profile);

routes.post('/create-session', passport.authenticate('local',{
    failureRedirect: '/login'
}),userController.createSession);


module.exports = routes;