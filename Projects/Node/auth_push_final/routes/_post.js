const express= require('express');

const route = express.Router();

const _postController= require('../controllers/_post_controller');

console.log("Post routing");

route.post('/postCreate', _postController.postCreate);

module.exports = route;