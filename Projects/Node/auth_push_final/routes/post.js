const express = require('express');
const passport = require('passport');

const routes = express.Router();

const postController = require('../controllers/post_controller');

console.log("Post Routing is called");

routes.post('/create', postController.createPost);

module.exports = routes;