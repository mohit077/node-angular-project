const user = require('../models/user');
const Post = require('../models/post');
const PostD = require('../models/_post');

module.exports.home = function(req,res){
   
    var user_id = req.user.id;
    Post.find({}).populate('user').exec(function(err,posts){
        return res.render('home',{
            posts : posts,
            user_id : user_id
        });
    });

    
}

module.exports.login = function(req,res){
    if(req.isAuthenticated())
    {
        return res.redirect('/users/profile');
    }
    return res.render('login');
}

module.exports.register = function(req,res){
    return res.render('register');
}

module.exports.registerUser = function(req,res){
    // user.create(req.body, function(err,userdata){
    //     if(err){
    //         console.log("record not inserted in db");
    //         return false;
    //     }
    //     console.log("record inserted into db");
    //     return res.redirect('back');
    //  })
        user.uploadedAvatar(req,res,function(err){
            if(err){
                console.log("file upload error");
            }
            user.username = req.body.username;
            user.email = req.body.email;
            user.password = req.body.password;
            if(req.file){
                // console.log(req.file);
                avatar = user.avatarPath+"/"+req.file.filename;
            }

            user.create({
                username : req.body.username,
                email : req.body.email,
                password : req.body.password,
                avatar : avatar
            });
            req.flash('success','Registration successfully');
            return res.redirect('back');
        });




}


module.exports.postPage = function(req,res){
   
    PostD.find({}).populate('user').exec(function(err,posts){
        return res.render('_home',{
            posts : posts,
            user_id : req.user._id
        });
    })
}