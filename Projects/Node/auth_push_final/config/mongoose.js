
const mongoose = require('mongoose');
const { devNull } = require('os');

mongoose.connect('mongodb://localhost/mdpogo');

const db = mongoose.connection;

db.on('error', console.error.bind(console,'error'));

db.once('open', function(err){
    if(err){
        console.log("Error is not connected");
        return false;
    }
    console.log("mongo db is connected");
});

module.exports = db;