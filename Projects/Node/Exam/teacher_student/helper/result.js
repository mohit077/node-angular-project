helper = [];
const Question = require("../models/question")
const User = require("../models/user")
const cron = require("node-cron");
const nodemailer = require("nodemailer");

// var transporter = nodemailer.createTransport({
//     host: "smtp.gmail.com",
//     port: 587,
//     secure: false,
//     requireTLS: true,
//     auth: {
//         user: 'mohitsakhiya1014@gmail.com',
//         pass: 'Mohit@$@1478'
//     }
// });

const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    ignoreTLS: false,
    secure: false,
    auth: {
        user: "developer.eww4@gmail.com",
        pass: 'Developer#321*'
    },
    tls: {
        rejectUnauthorized: false
    }
});

helper.check_value = async function check_value(result, n) {
    let mark = 0;
    for (var i = 0; i < n; i++) {
        var ans = quesResult[i].answer;
        console.log('ans :>> ', ans);
        var y = result.s_id;
        console.log(y);
        const findData = await Question.findById(y).lean().exec();
        // console.log('findData :>> ', findData);

        if (findData.answer == ans) {
            mark++;
        }
    }
    return mark;
};

helper.cron = (email, msg) => {


    console.log("running a task at every 5 minutes");
    const mailOptions = {
        from: process.env.USER_NAME,
        to: email,
        subject: "result",
        text: msg,
    };
    transporter.sendMail(mailOptions, (error, info) => {
        if (error) {
            console.log("Email not sent: " + error);
        } else {
            console.log("Email sent: " + info.response);
        }
    });

};

helper.detail = async(response) => {

    // console.log('response', response)
    for (let i = 0; i < response.length; i++) {
        const studentDetails = response[i]
        let studentResults = response[i].student_result;
        // console.log('studentResults', studentResults)

        let total = studentResults.length;
        let per = 0;
        for (let j = 0; j < total; j++) {
            per += studentResults[j].percentage;
        }
        const result = per / total;
        // console.log('result', result)
        if (!result) {
            continue;
        }
        const as = await User.findByIdAndUpdate(studentDetails._id, { result: result }).lean().exec();
        // console.log("as:" + as);
    }
}

helper.sendmail = () => {};

module.exports = helper;