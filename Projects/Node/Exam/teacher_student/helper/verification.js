let authHelper = [];
const { check } = require("express-validator");

authHelper.takeexamvalidation = [
    check("sub_id", "subject Id is required").notEmpty().exists(),
  ];

  module.exports = authHelper; 