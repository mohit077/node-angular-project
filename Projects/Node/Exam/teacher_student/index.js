require('dotenv').config();
const express = require('express');
const app = express()

const port = process.env.PORT;
const database = require('./database')
const userController = require('./controllers/user')
const examController = require('./controllers/exam')

app.use('/api/user', userController)
app.use('/api/exam', examController)

app.listen(port, () => {
    console.log("server running at", port)
})