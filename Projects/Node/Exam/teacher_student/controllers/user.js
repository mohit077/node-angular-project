const router = require('express').Router();
const bodyParser = require('body-parser')
const { validationResult } = require('express-validator')
const User = require('../models/user')
const jwt = require('jsonwebtoken');
const session = require('express-session')

// middleware setup
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(session({
    secret: "Secret key",
    resave: true,
    saveUninitialized: true
}));

router.post('/createnew', async (req, res) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(422).json({
            status: false,
            message: "form validation error",
            errors: errors.array()
        })
    }

    await User.create(req.body,
        (error, result) => {
            const { role } = req.body;

            if (error) {
                return res.json({
                    status: false,
                    message: `${role} Signup failed..`,
                    error
                })
            }
            return res.json({
                status: true,
                message: `${role} Signup successful..`,
                result
            })
        })
})

// router.get(
//     '/find',
//     (req, res) => {
//         User.find((error, result) => {
//             if (error) {
//                 return res.json({
//                     status: false,
//                     message: "user find fail..",
//                     error
//                 })
//             }
//             return res.json({
//                 status: true,
//                 message: "user find successful..",
//                 result
//             })
//         })
//     }
// )

const accessTokenSecret = 'youraccesstokensecret';

router.post('/login', async (req, res) => {
    // Read username and password from request body
    const { username, password } = req.body;

    // Filter user from the users by username and password
    const user = await User.findOne({ username: username, password: password }).lean().exec();
    console.log('user :>> ', user);

    if (user) {
        // Generate an access token
        const accessToken = jwt.sign({ username: user.username, role: user.role }, accessTokenSecret);

        res.json({
            message: "Login successfully...",
            accessToken
        });
    } else {
        res.send('Username or password incorrect');
    }
});

router.get('/logout', (req) => {
    // console.log('req.session :>> ', req.session);
    req.session.destroy(() => {
        console.log("user logged out");
    })
})

module.exports = router;