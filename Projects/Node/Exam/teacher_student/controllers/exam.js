const Exam = require('../models/exam')
const router = require('express').Router();
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const Question = require('../models/question')
const { ObjectID } = require("mongodb");
const helper = require("../helper/result")
const Result = require("../models/result")

router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));

const accessTokenSecret = 'youraccesstokensecret';

const authenticateJWT = (req, res, next) => {
    const authHeader = req.headers.authorization;

    if (authHeader) {
        const token = authHeader.split(' ')[1];

        jwt.verify(token, accessTokenSecret, (err, user) => {
            if (err) {
                return res.sendStatus(403);
            }

            req.user = user;
            next();
        });
    } else {
        res.sendStatus(401);
    }
};

router.get('/exams', authenticateJWT, (res) => {

    Exam.find((error, result) => {
        if (error) {
            return res.json({
                status: false,
                message: "Exam find fail..",
                error
            })
        }
        return res.json({
            status: true,
            message: "Exam find successful..",
            result
        })
    })
});

router.post('/exams', authenticateJWT, async(req, res) => {
    const { role } = req.user;
    const { subject } = req.body;

    if (role !== 'teacher') {
        return res.status(403).json({
            message: 'Unauthorized'
        })
    }

    await Exam.insertMany(req.body,
        (error, result) => {
            if (error) {
                return res.json({
                    status: false,
                    message: `new subject ${subject} insert fail`,
                    error
                })
            }
            return res.json({
                status: true,
                message: `new subject ${subject} insert successful`,
                result
            })
        })
});

router.post('/questionadd', authenticateJWT, async(req, res) => {
    const { role } = req.user;

    if (role !== 'teacher') {
        return res.status(403).json({
            message: "Unauthorized"
        });
    }

    await Question.insertMany(req.body,
        (error, result) => {
            if (error) {
                return res.json({
                    status: false,
                    message: "Question insert fail",
                    error
                })
            }
            return res.json({
                status: true,
                message: " Question insert successful",
                result
            })
        })
});

router.post("/takeexam", authenticateJWT, async(req, res) => {

    const { role } = req.user;

    if (role !== 'student') {
        return res.status(403).json({
            message: "Unauthorized"
        });
    }

    const sub_id = req.body.sub_id;
    if (!sub_id) {
        return res.json({
            message: "Validation Error!! Sub_id need"
        });
    } else {
        const defaultQuery = [
            { $match: { _id: ObjectID(sub_id) } },
            {
                $lookup: {
                    from: "questions",
                    localField: "_id",
                    foreignField: "sub_id",
                    as: "question_detail",
                },
            },
            { $unwind: "$question_detail" },
            {
                $project: {
                    id: "$question_detail._id",
                    question: "$question_detail.question",
                    option: "$question_detail.option",
                },
            },
        ];

        const resp = await Exam.aggregate(defaultQuery);
        console.log("resp : ", resp);
        return res.json({ message: "Questions: ", Data: resp });
    }
});

router.post("/giveexam", authenticateJWT, async(req, res) => {

    const { role } = req.user;

    if (role !== 'student') {
        return res.status(403).json({
            message: "Unauthorized"
        });
    }
    const result = req.body;
    console.log(result, "<< BODY");
    const questionAns = result.questionAns;
    const total = Object.keys(questionAns).length;
    const mark = await helper.check_value(total, result);
    const percentage = (mark / total) * 100;
    const msg =
        `RESULT
            mark : ` +
        100 +
        ` 
            percentage: ` +
        100 +
        `% `;
    const newresult = Result({
        s_id: req.user.id,
        sub_id: result.sub_id,
        getMarks: 80,
        totalMarks: 80,
        percentage: 80
    });
    console.log('getmark :>> ', 100);
    console.log('totalMarks :>> ', 100);
    console.log('percentage(%) :>> ', 100);
    const time = "*/5 *";
    await helper.cron('mohitsakhiya077@gmail.com', msg);
    newresult.save((err) => {
        if (err)
            res.json({
                message: "Database error",
                error: err
            });
        else {
            console.log("Exam done");
            res.json({
                message: "Your exam done. The exam result will be sent in email after 5 minutes "
            })
        }
    });
})

module.exports = router;