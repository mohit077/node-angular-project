const { ObjectID } = require('mongodb');
const mongoose = require('mongoose');

const ResultSchema = mongoose.Schema({
    s_id: ObjectID,
    sub_id: ObjectID,
    questionAns: [],
    percentage: Number,
    getMarks: Number,
    totalMarks: Number
});

module.exports = mongoose.model("Result", ResultSchema);