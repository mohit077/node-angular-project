const mongoose = require('mongoose')

const userSchema = mongoose.Schema({

    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },

    role: {
        type: String,
        required: true
    },

    result: Number
})

mongoose.model('users', userSchema)

module.exports = mongoose.model('users')
