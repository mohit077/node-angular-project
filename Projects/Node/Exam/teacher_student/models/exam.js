const mongoose = require('mongoose')

const examSchema = mongoose.Schema({

    subject: {
        type: String,
        required: true
    }
})

mongoose.model('exams', examSchema)

module.exports = mongoose.model('exams')