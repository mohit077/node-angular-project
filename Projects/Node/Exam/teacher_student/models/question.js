const mongoose = require('mongoose')
const { ObjectID } = require('mongodb');

const questionSchema = mongoose.Schema({

    sub_id : ObjectID,
    question: {
        type: String,
        required: true
    },
    option: [],
    answer: {
        type: String,
        required: true
    }
})

mongoose.model('question', questionSchema)

module.exports = mongoose.model('question')
