const Admin = require("../model/admin");
const Message = require("../model/message");
let _ = require("underscore");

const { StatusCodes } = require("http-status-codes");
const { result } = require("underscore");

exports.userList = async (req, res) => {
  const ListOfUser = await Admin.find({ role: "user" });
  // console.log(ListOfUser);
  res.status(StatusCodes.CREATED).json({
    msg: "All User Get!!!",
  });
};

exports.UserLogin = async (req, res) => {
  const { email, password } = req.body;

  Admin.findOne({ email: email }, async (err, user) => {
    if (!email || !password) {
      res.status(400).json({
        msg: "Please provide email and password",
      });
    }

    const User = await Admin.findOne({ email });
    if (!User) {
      res.status(401).json({
        msg: "Invalid Credentials",
      });
    }

    const isPasswordCorrect = await Admin.findOne({ password });
    if (!isPasswordCorrect) {
      res.status(401).json({
        msg: "Invalid password",
      });
    }

    Admin.updateMany(
      { status: "active" },
      { status: "unactive" },
      function (err, docs) {
        if (err) {
          console.log(err);
        } else {
          console.log("UpdateMany : ", docs);
        }
      }
    );

    var userId = user.id;

    Admin.findByIdAndUpdate(userId, { status: "active" }, function (err, docs) {
      if (err) {
        console.log(err);
      } else {
        console.log("Updated User Status:");
      }
    });
  });

  res.status(StatusCodes.OK).json({
    msg: "User Login Successfully",
  });
};

exports.UserChatList = async (req, res) => {
  const ListOfUser = await Admin.find({ status: "active" });
  if (!ListOfUser) {
    res.status(StatusCodes.OK).json({
      msg: "Not Match Login User Details",
    });
  }
  // res.status(StatusCodes.OK).json(ListOfUser[0]);

  const LoginUserId = ListOfUser[0].id;

  console.log(LoginUserId, "LoginUserId");

  const SenderId = Message.find(
    { sender_id: LoginUserId },null,{ sort :{  createdAt: -1 }},
    function (err, results) {
      console.log(results, "Send Message");

      const all_sender_id = _.pluck(results, "sender_id");
      const all_sender_receiver_id = _.pluck(results, "receiver_id");

      const unique_sender_id = _.union(all_sender_id);
      const messageSenddata = Admin.find(
        { _id: unique_sender_id },
        function (err, mesSend) {
          console.log(mesSend, "Send = MessageSenderData ( sender_id )");
        }
      )

      const messagereceiverdata = Admin.find(
        { _id: all_sender_receiver_id },
        function (err, mesRec) {
          console.log(mesRec, "Receiver = MessageReceiverData ( receiver_id )"); 
        }
      );
    }
  );

  const RecieverId = Message.find(
    { receiver_id: LoginUserId },null,{ sort :{  createdAt: -1 }},
    function (err, results) {
      console.log(results, "Received Message");

      const all_sender_id = _.pluck(results, "sender_id");
      const all_receiver_id = _.pluck(results, "receiver_id");

    let unique_sender_id = _.union(all_sender_id);
    let unique_rec_id = _.union(all_receiver_id);

      const messageSenderdata = Admin.find({ _id: unique_sender_id },
        function (err, mesSend) {
          console.log(mesSend, "Received = MessageSenderData ( sender_id )");
        }
        ).sort({createdAt:-1});

        const messageRecdata = Admin.find(
            { _id: unique_rec_id },
            function (err, mesRec) {
            console.log(mesRec, "Received = MessageReceiverData ( receiver_id )");
            }
        ); 
    }
  );

  res.status(200).json({
    msg: "User Chat List Show (console)",
  });

};

exports.UserChat = async (req, res) => {

  const UserId = req.params.id;

  console.log(UserId, "UserId");

  const ListOfLoginUser = await Admin.findOne({ status: "active" });
  if (!ListOfLoginUser) {
    res.status(StatusCodes.OK).json({
      msg: "Not Match Login User Details",
    });
  }

  const LoginUserdetails = ListOfLoginUser.id;

  console.log(LoginUserdetails, "LoginUserdetails");

  const LoginUserChatSender = Message.find({ sender_id: LoginUserdetails , receiver_id: UserId },null,{ sort :{  createdAt: -1 }}, function(err, docs) {
    console.log(docs, "LoginUserChatSender");
  });


  const LoginUserChatReceiver = Message.find({ sender_id: UserId , receiver_id: LoginUserdetails },null,{ sort :{  createdAt: -1 }}, function(err, docs) {
    console.log(docs, "LoginUserChatReceiver");
  });
 
  // const UserChatSender = Message.find({ sender_id: UserId }, function(err, docs) {
  //   console.log(docs, "UserChatSender");
  // });


  // const UserChatReceiver = Message.find({ receiver_id: UserId }, function(err, docs) {
  //   console.log(docs, "UserChatReceiver");
  // });
  
  // const SenderId = Message.find(
  //   { sender_id: LoginUserId },null,{ sort :{  createdAt: -1 }},
  //   function (err, results) {
  //     _.each(results, singleData=>{
  //      console.log(singleData.message, " == message list by sender")
  //     })
  //     console.log(results, "Send Message");
  //   }
  // );

  // const RecieverId = Message.find(
  //   { receiver_id: LoginUserId },null,{ sort :{  createdAt: -1 }},
  //   function (err, results) {
  //     _.each(results, singleData=>{
  //       console.log(singleData.message, "== message list by reciver")
  //      })
  //     console.log(results, "Received Message");
  //   }
  // );



}