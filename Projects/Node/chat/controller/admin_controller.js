const Admin = require("../model/admin");
const { StatusCodes } = require("http-status-codes");

exports.register = async (req, res) => {
  const { name, email, password } = req.body;

  const emailAlreadyExist = await Admin.findOne({ email });
  if (emailAlreadyExist) {
    res.status(409).send("Email Already Exist");
  }

  const isFirstAccount = (await Admin.countDocuments({})) === 0;

  const role = isFirstAccount ? "admin" : "user";

  const user = await Admin.create({
    name,
    email,
    password,
    role,
    user_id: Math.floor(Math.random() * 100) + 2,
  });

  res.status(StatusCodes.CREATED).json({
    msg: "Register Success!",
  });
};
