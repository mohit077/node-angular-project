const Message = require("../model/message");
const Admin = require("../model/admin");
const { StatusCodes } = require("http-status-codes");

exports.SendMessage = async (req, res) => {
  const { sender_id, receiver_id, message } = req.body;

  const FindBySenderId = await Admin.findById({ _id: sender_id });
  if (!FindBySenderId) {
    res.status(403).json({
      msg: "Sender ID Your Id Incorrect Please Enter valid ID",
    });
  }

  const FindByReceiverId = await Admin.findById({ _id: receiver_id });
  if (!FindByReceiverId) {
    res.status(403).json({
      msg: "Receiver ID incorrect Please select the valid user",
    });
  }

  const MessageRecord = await Message.create({
    sender_id,
    receiver_id,
    message,
  });
  console.log(MessageRecord, "Message Create");

  res.status(StatusCodes.OK).json({
    msg: "Message Send Succesfully",
  });
};
