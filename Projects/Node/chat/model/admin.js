const mongoose = require("mongoose");
const validator = require("validator");
const bcrypt = require("bcrypt");
const passport = require("passport");

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, "Please provide name"],
    minlength: 3,
    maxlength: 50,
  },
  email: {
    type: String,
    unique: true,
    required: [true, "Please provide email"],
    validate: {
      validator: validator.isEmail,
      message: "Please provide valid email",
    },
  },
  password: {
    type: String,
    required: [true, "Please provide password"],
    minlength: 6,
  },
  role: {
    type: String,
    enum: ["admin", "user"],
    default: "user",
  },
  user_id: {
    type: String,
  },
  status: {
    type: String,
    enum: ["active", "unactive"],
    default: "unactive",
  },
});

module.exports = mongoose.model("Admin", UserSchema);
