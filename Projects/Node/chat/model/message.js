const mongoose = require("mongoose");

const MessageSchema = new mongoose.Schema(
  {
    sender_id: {
      type: String,
      required: true,
    },
    receiver_id: {
      type: String,
      required: true,
    },
    message: {
      type: String,
      required: true,
    },
    read_unread: {
      type: String,
      timestamps: Date.now(),
      default: false
    },
    created_at:{
        type: Date,
        default: Date.now()
      }
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Message", MessageSchema);
