const express = require("express");
const port = 3002;
const app = express();
const db = require("./config/mongoose");

const session = require("express-session");
const passport = require("passport");
const passportLocal = require("./config/passport-local");

app.use(express.json());

app.use("/admin", require("./routes/admin"));
app.use("/message", require("./routes/message"));
app.use("/user", require("./routes/user"));

app.listen(port, function (err) {
  if (err) {
    console.log(err);
  }
  console.log("server is running on port", port);
});
