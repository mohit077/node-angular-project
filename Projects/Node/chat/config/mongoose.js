const mongoose = require("mongoose");

mongoose.connect("mongodb://localhost/ChatApp");

const db = mongoose.connection;

db.on("error", console.error.bind(console, "error"));

db.once("open", function (err) {
  if (err) {
    console.log("db is not connected ❌");
    return false;
  }
  console.log("connection ✅");
});

module.exports = db;
