const express = require("express");
const router = express.Router();

const message_controller = require("../controller/message_controller");

router.post("/sendmessage", message_controller.SendMessage);

module.exports = router;
