const express = require("express");
const router = express.Router();

const admin_controller = require("../controller/admin_controller");

router.post("/register", admin_controller.register);

module.exports = router;
