const express = require("express");

const router = express.Router();

const user_controller = require("../controller/user_controller");

router.get("/userlist", user_controller.userList);
router.post("/userlogin", user_controller.UserLogin);
router.get("/userchatlist", user_controller.UserChatList);
router.get('/userchat/:id', user_controller.UserChat);

module.exports = router;


